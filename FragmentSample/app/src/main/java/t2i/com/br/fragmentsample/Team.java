package t2i.com.br.fragmentsample;

import java.io.Serializable;

/**
 * Created by wallace on 16/12/17.
 */

/**
 *  Classe modelo de time.
 *
 */
public class Team implements Serializable{

    private String name;
    private int imageID;

    public Team(String name, int imageID) {
        this.imageID = imageID;
        this.name = name;
    }


    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
