package br.com.dts.webservices;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.dts.webservices.model.Place;

/**
 * Created by diegosouza on 8/3/16.
 */

public class PlaceDetailActivity extends AppCompatActivity {

    public static final int MY_PERMISSIONS_REQUEST_ACCESS_MAP = 1;

    //public static final String EXTRA_PLACE_ROUTE = "place_route";
    private TextView mTextName;
    private TextView mTextDetail;

    private GoogleMap mMap;

    private Place mPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);

        mPlace = (Place) getIntent().getSerializableExtra(ListPlacesActivity.EXTRA_PLACE);

        mTextName = (TextView) findViewById(R.id.txt_place_name);
        mTextDetail = (TextView) findViewById(R.id.txt_place_details);

        checkPermission();


        if (mPlace != null){
            mTextName.setText(mPlace.getProperties().getPTurist());
            mTextDetail.setText(mPlace.getProperties().getDescritv());
        }
    }

    private void setupMap(){

        MapFragment fragment = ((MapFragment)getFragmentManager().findFragmentById(R.id.mapview));

        fragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mMap.setMyLocationEnabled(true);

                double latitude = Double.parseDouble(mPlace.getGeometry().getCoordinates()[1]);
                double longitude = Double.parseDouble(mPlace.getGeometry().getCoordinates()[0]);


                LatLng placeCoords = new LatLng(latitude, longitude);
                mMap.addMarker(new MarkerOptions().position(placeCoords)
                        .title(mPlace.getProperties().getPTurist()));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(placeCoords,15));
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_route:
                Intent intent = new Intent(this, TraceRouteActivity.class);
                intent.putExtra(ListPlacesActivity.EXTRA_PLACE, mPlace);
                startActivity(intent);

                break;
            case R.id.external_maps:
                double latitude = Double.parseDouble(mPlace.getGeometry().getCoordinates()[1]);
                double longitude = Double.parseDouble(mPlace.getGeometry().getCoordinates()[0]);

                //Uri mapUri = Uri.parse("geo:0,0?q=37.423156,-122.084917 (" + "Local" + ")");

                Uri mapUri = Uri.parse("geo:0,0?q="+latitude + "," + longitude + "(" + mPlace.getProperties().getPTurist() + ")");
                Intent it = new Intent(Intent.ACTION_VIEW, mapUri);
                //Use it to open google Maps app directly
                it.setPackage("com.google.android.apps.maps");
                startActivity(it);


                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void checkPermission() {

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                setupMap();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_MAP);
            }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_MAP: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupMap();
                } else {

                }
                return;
            }
        }
    }


}
